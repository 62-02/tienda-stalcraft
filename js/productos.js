 // Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.2/firebase-app.js";
import { getDatabase, onValue, ref, set, child, get, update, remove} from "https://www.gstatic.com/firebasejs/9.17.2/firebase-database.js";
import { getStorage,ref as refStorage, uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.17.2/firebase-storage.js";

const firebaseConfig = {
    apiKey: "AIzaSyDu5m_xRjf7XLTE3BYlkDpZCj4ZT5NJ1Gk",
    authDomain: "proyecto-patrick.firebaseapp.com",
    databaseURL: "https://proyecto-patrick-default-rtdb.firebaseio.com",
    projectId: "proyecto-patrick",
    storageBucket: "proyecto-patrick.appspot.com",
    messagingSenderId: "520991521036",
    appId: "1:520991521036:web:a153b4b03bad63ea0eb129",
    measurementId: "G-VYWMNNQYY0"
};

  // Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();
const storage = getStorage();

// Generar Productos
let productos = document.getElementById('contenido-productos');
window.addEventListener('DOMContentLoaded',mostrarProductos);
let comun = document.getElementById('Comun');
let pocoComun = document.getElementById('PocoComun');
let raro = document.getElementById('Raro');
let epico = document.getElementById('Epico');
let exotico = document.getElementById('Exotico');
let legendario = document.getElementById('Legendario');
let todos = document.getElementById('todos');

function mostrarProductos(){
    const dbRef = ref(db, "productos");


    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();
        
        if(childData.estatus=="1"){

            productos.innerHTML +=`
                <div class='producto'>
                <img class='img-item' src='${childData.urlImagen}'>
                <p class='nombre'>${childData.nombre}</p>
                <p class='descripcion' style='font-size: .9em;'>${childData.descripcion}</p>
                <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                <p class='precio'>\$${childData.precio}</p>
                <button class='boton-comprar' data-bs-toggle="modal" data-bs-target="#exampleModalToggle">Comprar</button>
                </div>
            `;
        }
        
        });
    },
    {
        onlyOnce: true,
    }
    );
}




function mostrarComun(){
    const dbRef = ref(db, "productos");
    
    document.getElementById('tituloProductos').innerHTML = "Comun";
    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childData = childSnapshot.val();
            
            
            if(childData.categoria==1 && childData.estatus=="1"){
                productos.innerHTML +=`
                    <div class='producto'>
                    <img class='img-item' src='${childData.urlImagen}'>
                    <p class='nombre'>${childData.nombre}</p>
                    <p class='descripcion' style='font-size: .9em;'>${childData.descripcion}</p>
                    <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                    <p class='precio'>\$${childData.precio}</p>
                    <button class='boton-comprar'>Comprar</button>
                    </div>
                `;
                
            }
            
            
        });
    },
    
    {
        onlyOnce: true,
    }
    );
} 


function mostrarPocoComun(){
    const dbRef = ref(db, "productos");
    
    document.getElementById('tituloProductos').innerHTML = "Poco Comun";

    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
            snapshot.forEach((childSnapshot) => {
                const childData = childSnapshot.val();
                
                
                if(childData.categoria==2 && childData.estatus=="1"){
                    productos.innerHTML +=`
                        <div class='producto'>
                        <img class='img-item' src='${childData.urlImagen}'>
                        <p class='nombre'>${childData.nombre}</p>
                        <p class='descripcion' style='font-size: .9em;'>${childData.descripcion}</p>
                        <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                        <p class='precio'>\$${childData.precio}</p>
                        <button class='boton-comprar'>Comprar</button>
                        </div>
                    `;
                    
                }
                
                
            });
    },
    
    {
        onlyOnce: true,
    }
    );
    
} 

function mostrarRaro(){
    const dbRef = ref(db, "productos");
    
    document.getElementById('tituloProductos').innerHTML = "Raro";
    
    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childData = childSnapshot.val();
            

                if(childData.categoria==3 && childData.estatus=="1"){
                    productos.innerHTML +=`
                        <div class='producto'>
                        <img class='img-item' src='${childData.urlImagen}'>
                        <p class='nombre'>${childData.nombre}</p>
                        <p class='descripcion' style='font-size: .9em;'>${childData.descripcion}</p>
                        <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                        <p class='precio'>\$${childData.precio}</p>
                        <button class='boton-comprar'>Comprar</button>
                        </div>
                    `;
                    
                }
                
                
            });
        },
        
        {
            onlyOnce: true,
        }
        );
        
    } 

function mostrarEpico(){
    const dbRef = ref(db, "productos");

    document.getElementById('tituloProductos').innerHTML = "Epico";
    
    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childData = childSnapshot.val();
            
            
            if(childData.categoria==4 && childData.estatus=="1"){
                productos.innerHTML +=`
                    <div class='producto'>
                    <img class='img-item' src='${childData.urlImagen}'>
                    <p class='nombre'>${childData.nombre}</p>
                    <p class='descripcion' style='font-size: .9em;'>${childData.descripcion}</p>
                    <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                    <p class='precio'>\$${childData.precio}</p>
                    <button class='boton-comprar'>Comprar</button>
                    </div>
                `;
                
            }
            
            
        });
    },
    
    {
        onlyOnce: true,
}
);

} 

function mostrarExotico(){
    const dbRef = ref(db, "productos");
    
    document.getElementById('tituloProductos').innerHTML = "Exotico";

    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childData = childSnapshot.val();
            
            
            if(childData.categoria==5 && childData.estatus=="1"){
                productos.innerHTML +=`
                    <div class='producto'>
                    <img class='img-item' src='${childData.urlImagen}'>
                    <p class='nombre'>${childData.nombre}</p>
                    <p class='descripcion' style='font-size: .9em;'>${childData.descripcion}</p>
                    <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                    <p class='precio'>\$${childData.precio}</p>
                    <button class='boton-comprar'>Comprar</button>
                    </div>
                `;
                
            }
            
            
        });
    },
    
    {
        onlyOnce: true,
    }
    );
    
}

function mostrarLegendario(){
    const dbRef = ref(db, "productos");
    
    document.getElementById('tituloProductos').innerHTML = "Legendario";

    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childData = childSnapshot.val();
            
            
            if(childData.categoria==6 && childData.estatus=="1"){
                productos.innerHTML +=`
                    <div class='producto'>
                    <img class='img-item' src='${childData.urlImagen}'>
                    <p class='nombre'>${childData.nombre}</p>
                    <p class='descripcion' style='font-size: .9em;'>${childData.descripcion}</p>
                    <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                    <p class='precio'>\$${childData.precio}</p>
                    <button class='boton-comprar'>Comprar</button>
                    </div>
                `;
                
            }
            
            
        });
    },
    
    {
        onlyOnce: true,
    }
    );
    
}


function mostrarTodos(){
    const dbRef = ref(db, "productos");

    document.getElementById('tituloProductos').innerHTML = "Productos Actuales";

    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();
        
        if(childData.estatus=="1"){
            productos.innerHTML +=`
                <div class='producto'>
                <img class='img-item' src='${childData.urlImagen}'>
                <p class='nombre'>${childData.nombre}</p>
                <p class='descripcion' style='font-size: .9em;'>${childData.descripcion}</p>
                <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                <p class='precio'>\$${childData.precio}</p>
                <button class='boton-comprar'>Comprar</button>
                </div>
            `;
        }
        
        });
    },
    {
        onlyOnce: true,
    }
    );
}

comun.addEventListener('click', mostrarComun);
pocoComun.addEventListener('click', mostrarPocoComun);
raro.addEventListener('click', mostrarRaro);
epico.addEventListener('click', mostrarEpico);
exotico.addEventListener('click', mostrarExotico);
legendario.addEventListener('click', mostrarLegendario);
todos.addEventListener('click', mostrarTodos);
