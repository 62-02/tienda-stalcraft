  // Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.2/firebase-app.js";
import { getAuth, signInWithEmailAndPassword, signOut, onAuthStateChanged } from "https://www.gstatic.com/firebasejs/9.17.2/firebase-auth.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

const firebaseConfig = {
    apiKey: "AIzaSyDu5m_xRjf7XLTE3BYlkDpZCj4ZT5NJ1Gk",
    authDomain: "proyecto-patrick.firebaseapp.com",
    databaseURL: "https://proyecto-patrick-default-rtdb.firebaseio.com",
    projectId: "proyecto-patrick",
    storageBucket: "proyecto-patrick.appspot.com",
    messagingSenderId: "520991521036",
    appId: "1:520991521036:web:a153b4b03bad63ea0eb129",
    measurementId: "G-VYWMNNQYY0"
};

 // Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);




// Iniciar Sesión
function login(){
    event.preventDefault();
    let email = document.getElementById('email').value;
    let contra = document.getElementById('contraseña').value;

    if(email == "" || contra == ""){
        alert('Complete los campos');
        return;
    }
    const auth = getAuth();
    signInWithEmailAndPassword(auth, email, contra).then((userCredential) => {
    alert('Bienvenido ' + email);
    sessionStorage.setItem('isAuth',"true");
    window.location.href = 'administrador.html';
    
    })
    .catch((error) => {
        alert('Usuario y o contraseña incorrectos')
    });

}



var btnCerrarSesion = document.getElementById('btnCerrarSesion');

if(btnCerrarSesion){
    btnCerrarSesion.addEventListener('click',  (e)=>{
        signOut(auth).then(() => {
        alert("SESIÓN CERRADA")
        window.location.href="login.html";
        // Sign-out successful.
        }).catch((error) => {
        // An error happened.
        });
    });
}

onAuthStateChanged(auth, async user => {
    console.log(window.location.pathname);
    if (user) {
        // if (window.location.pathname.includes("login")) {
        //     window.location.href = "/html/administrador.html";
        // }
    } else {
        if (window.location.pathname.includes("administrador")) {
            window.location.href = "/login.html";
        }
    }
});

var botonLogin = document.getElementById('btnIniciarSesion');

if(botonLogin){
    botonLogin.addEventListener('click', login);
}